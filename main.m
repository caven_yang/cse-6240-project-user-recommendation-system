fprintf('this is the project\n');
% clearvars
addpath('data/','functions/');

global TOT_RATER TOT_MOVIE
TOT_RATER = 10681;TOT_MOVIE = 71567;
% TOT_RATER = 100; TOT_MOVIE = 100;


global train test

train = loadRatings('data/u1.base');
test = loadTest('data/u1.test');


global K M

K = 5;
M = 40;

global Qu Qv
Qu = struct('mean',randi(1,K,TOT_RATER)-1, 'var', randi(1,K,1)/100);
Qv = struct('mean',randi(1,K,TOT_MOVIE)-1, 'var', randi(1,K,1)/100);



for iteration = 1:1:90
    fprintf( 'for iteration %i\n', iteration);
%     error_bar = [error_bar;errorBar()];
    
    if(iteration > 99)
        getSIGMA();
        fprintf('\tSIGMA obtained\n');    
        getDeltaList(); 
    end
    
    batch = getBatch(); % updates active at the same time
    
    updateParam_();
    
    
    
    
end