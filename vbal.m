fprintf('this is the project\n');
% clearvars
addpath('data/','functions/');

global TOT_RATER TOT_MOVIE
% TOT_RATER = 6040;TOT_MOVIE = 3952;
TOT_RATER = 943;TOT_MOVIE = 1682;

global a b c d f B C
global train test

train = loadRatings_('data/u1.base');
test = loadTest_('data/u1.test');

% train = loadRatings('data/m.train');
% test = loadTest('data/m.test');

fprintf('loading done\n');

global K M

K = 5;
M = 100;

global Qu Qv
Qu = struct('mean',randi(1,K,TOT_RATER), 'var', randi(100,K,TOT_RATER)/100);
Qv = struct('mean',randi(1,K,TOT_MOVIE)-1, 'var', randi(100,K,TOT_MOVIE)/100);

global delta_list batch active
delta_list = struct('u', randi(100,1, TOT_RATER), 'v', randi(100,1,TOT_MOVIE));
batch = struct('u',[], 'v', []);
active = struct('u',[],'v',[]);

global Vx Vs
Vx = 1;
Vs = ones(1,K);

global eta decay
eta = -0.01;decay = 0.99;

error_bar = [];

global sampling;
sampling = [];

t1=clock;
for epoch=1:1:1000
    fprintf('for epoch %d\n',epoch);
    error_bar = [error_bar;errorBar_()];
    
    active.u = []; active.v = [];
    delta_list.u = randi(100,1,TOT_RATER); delta_list.v = randi(100,1,TOT_MOVIE);
    for iteration = 1:1:100000
        fprintf( 'for iteration %i\n', iteration);
%         error_bar = [error_bar;errorBar_()];
        errorBar_();
        if(iteration > 1)
           updateDeltaList(); 
        end
        getBatch();  % update active meanwhile
%         fprintf('active size is %d\tbatch size is %d\n', length(active.u), length(batch.u));
        if( length(active.u) > min(TOT_MOVIE,TOT_RATER) )
           break; 
        end
        
        updateParam();
%         eta = eta*decay;

    end
    
end

t2=clock;
fprintf('time used is %1.4f\n', etime(t2,t1));
