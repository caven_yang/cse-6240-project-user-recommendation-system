function k=rowKernel(x,y)

k=exp(uDkl(x,y) + uDkl(x,y));