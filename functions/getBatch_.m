function ind = getBatch_()

global delta_list batch active

global M sampling train ACT


sorted = sort(delta_list.u,'descend');

chosen1 = find(delta_list.u >= sorted(M));
chosen2 = find(delta_list.u > sorted(M));

if(length(chosen1) == M)
    batch.u = chosen1;
else
    diff = setdiff(chosen1,chosen2);
    batch.u = [chosen2, diff(1:M-length(chosen2))];
end


sorted = sort(delta_list.v, 'descend');

chosen1 = find(delta_list.v >= sorted(M));
chosen2 = find(delta_list.v > sorted(M));

if(length(chosen1) == M)
    batch.v = chosen1;
else
    diff = setdiff(chosen1,chosen2);
    batch.v = [chosen2, diff(1:M-length(chosen2))];
end


% delta_list.u(batch.u) = -10;
% delta_list.v(batch.v) = -10;
% 
% active.u = sort([active.u, batch.u]);
% active.v = sort([active.v, batch.v]);


for x=1:1:length(batch.u)
   i = batch.u(x);
   for y = 1:1:length(batch.v)
       j=batch.v(y);
       r = train.rating(i,j);
       if( r > 0)           
          ACT = [ACT;i,j]; 
       end    
   end
end



for x =1:1:length(batch.u)
    i=batch.u(x);
    if( length(find(ACT(:,1) == i)) >= length(find(train.rating(i,:) > 0))/2 )
        delta_list.u(i) = -10;
        active.u = [active.u,i];
    
    end
end

for y=1:1:length(batch.v)
    j= batch.v(y);
    if( length(find(ACT(:,2) == j)) >= length(find(train.rating(:,j) > 0))/2)
       delta_list.v(j) = -10; 
       active.v = [active.v,j];
    end
end


active.u = sort(active.u);
active.v = sort(active.v);







tot = 0;
for x=1:1:length(batch.u)
    i=batch.u(x);
    for y = 1:1:length(batch.v)
        j=batch.v(y);
        r = train.rating(i,j);
        if(r>0)
            tot = tot + 1;
        end
    end
end

sampling=[sampling,tot];

ind = 1;