function r = findRating(p,q)

global train


pset=find(train(:,1) == p);
qset=find(train(:,2) == q);

rset = intersect(pset,qset);



if( isempty(rset) )
    
    r = 0;
else
    r=train(rset,3);
end


