function list = getDeltaList()

global delta_list SIGMA active

global TOT_RATER TOT_MOVIE

global U V
delta_list.u = ones(1,TOT_RATER)*-100;
delta_list.v = ones(1,TOT_MOVIE)*-100;

for i=1:1:TOT_RATER
    if( ismember(i, active.u) == 1 ) % i is in active.u
        fprintf('\t%d is in active.u\tdelta = %1.2f\n',i, delta_list.u(i));
        continue;
    end
    
    delta_list.u(i) = getDelta(i,U);
    fprintf('\t%d is not in active.u\tdelta = %1.2f\n',i,delta_list.u(i));
end




for i=1:1:TOT_MOVIE
    if( ismember(i, active.v) == 1 ) % i is in active.u
        fprintf('\t%d is in active.v\tdelta = %1.2f\n',i, delta_list.v(i));
        continue;
    end
    
    delta_list.v(i) = getDelta(i,V);
    fprintf('\t%d is not in active.v\tdelta = %1.2f\n',i,delta_list.v(i));
end