function ind = updateParameters()
global eta decay

global sigma_epsilon TAO kappa Rho_ksi

global Qu Qv batch K train pref pref_var

global TOT_RATER TOT_MOVIE

C_mean_u = zeros(K,TOT_RATER);

C_mean_v = zeros(K,TOT_MOVIE);

C_mean_pref_u = zeros(1,TOT_RATER);

C_mean_pref_v = zeros(1,TOT_MOVIE);

%updating C_mean_u


for k=1:1:K
    for s=1:1:length(batch.u)
        i = batch.u(s);
        val = Qu.mean(k,i);
        for t = 1:1:length(batch.v)
            j = batch.v(t);
            r = train.rating(i,j);
            if( r > 0)
%                 fprintf('\nrating is %1.3f\t', r);
                tmp = double(r) - normrnd(pref.u(i), pref_var.u(i)) - normrnd(pref.v(j), pref_var.v(j));
                tmp = tmp - (Qu.mean(:,i))'*(Qv.mean(:,j));
%                 fprintf('error is %1.3f\n', tmp);
                tmp = -1*tmp*Qv.mean(k,j);
                tmp = tmp + Qu.mean(k,i)*Qv.var(k,j);
                val = val + tmp/sigma_epsilon;
%                 fprintf('long term is %1.3f\tadding to %1.3f\n', tmp, val);
            end
        end
        C_mean_u(k,i) = val;
%         fprintf('C_mean_u(%d,%d) is %1.3f\n', k, s, val);
    end
end



for k=1:1:K
    for t=1:1:length(batch.v)
        j=batch.v(t);
        val=Qv.mean(k,j)*gamrnd(Rho_ksi.a(k),1.0/Rho_ksi.b(k));
        for s=1:1:length(batch.u)
            i = batch.u(s);
            r = train.rating(i,j);
            if( r > 0)
               tmp = double(r) - normrnd(pref.u(i), pref_var.u(i)) - normrnd(pref.v(j),pref_var.v(j)); 
               tmp = tmp - (Qu.mean(:,i))'*(Qv.mean(:,j));
               tmp = -1*tmp*Qu.mean(k,i) + Qv.mean(k,j)*Qu.var(k,i);
               val = val + tmp/sigma_epsilon;
                
            
            
            end
        end
        C_mean_v(k,j) = val;
%         fprintf('C_mean_v(%d,%d) is %1.3f\n', k, t, val);
    end
end


Qu.mean = Qu.mean + eta*C_mean_u;
Qv.mean = Qv.mean + eta*C_mean_v;



for t=1:1:length(batch.u)
    i = batch.u(t);
    val = pref.u(i)/TAO.a;
    for s=1:1:length(batch.v)
        j = batch.v(s);
        r = train.rating(i,j);
        if( r>0)
            tmp = double(r) - normrnd(pref.u(i), pref_var.u(i)) - normrnd(pref.v(j),pref_var.v(j)); 
            tmp = tmp - (Qu.mean(:,i))'*(Qv.mean(:,j)); 
            tmp = -1*tmp/sigma_epsilon;
            val = val + tmp;
        end
    end
    C_mean_pref_u(1,i) = val;
%     fprintf('C_mean_pref_u(%d) is %1.3f\n', t, val);
end


for s=1:1:length(batch.v)
    j = batch.v(s);
    val = pref.v(j)/TAO.b;
    for t=1:1:length(batch.u)
        i = batch.u(t); 
        r = train.rating(i,j);
        if( r > 0)
            tmp = double(r) - normrnd(pref.u(i), pref_var.u(i)) - normrnd(pref.v(j),pref_var.v(j)); 
            tmp = tmp - (Qu.mean(:,i))'*(Qv.mean(:,j)); 
            tmp = -1*tmp/sigma_epsilon;
            val = val + tmp;
            
        end
       
    end
    C_mean_pref_v(1,j) = val;
%     fprintf('C_mean_pref_v(%d) is %1.3f\n', s, val);
    
end

pref.u = pref.u + eta*C_mean_pref_u;
pref.v = pref.v + eta*C_mean_pref_v;

S_var_u = zeros(K,TOT_RATER);
S_var_v = zeros(K,TOT_MOVIE);
S_var_pref_u = zeros(1,TOT_RATER);
S_var_pref_v = zeros(1,TOT_MOVIE);





for k=1:1:K
    for x =1:1:length(batch.u)
        i = batch.u(x);
        val = 1;
        for y=1:1:length(batch.v)
            j = batch.v(y);
            r = train.rating(i,j);
            if( r > 0)
                tmp = Qv.var(k,j) + (Qv.mean(k,j))^2;
                val = val + tmp/sigma_epsilon;
            end

        end
        S_var_u(k,i) = 1.0/val;
    end
end


for k=1:1:K
    for y = 1:1:length(batch.v)
        j = batch.v(y);
        val = 1*gamrnd(Rho_ksi.a(k),1.0/Rho_ksi.b(k));
        for x = 1:1:length(batch.u)
            i = batch.u(x);
            r = train.rating(i,j);
            if( r > 0)
                tmp = Qu.mean(k,i);
                tmp = tmp*tmp;
                tmp = tmp + Qu.var(k,i);
                val = val + tmp/sigma_epsilon;
            end
        end
        S_var_v(k,j) = 1.0/val;
    end
end

Qu.var = (1-eta)*Qu.var + eta*S_var_u;
Qv.var = (1-eta)*Qv.var + eta*S_var_v;


for x= 1:1:length(batch.u)
   i = batch.u(x);
   val = 1.0/TAO.a;
   for y = 1:1:length(batch.v)
    j = batch.v(y);
    r = train.rating(i,j);
    if( r > 0)
       val = val + 1/sigma_epsilon; 
    
    end
   end
   
   S_var_pref_u(i) = 1.0/val;
end

for y=1:1:length(batch.v)
   j = batch.v(y);
   val = 1.0/TAO.b;
   for x = 1:1:length(batch.u)
    i = batch.u(x);
    r = train.rating(i,j);
    if( r > 0)
        val = val + 1/sigma_epsilon;
        
    end
    
   end
   S_var_pref_v(j) = 1.0/val;
end


pref_var.u = (1-eta)*pref_var.u + eta*S_var_pref_u;
pref_var.v = (1-eta)*pref_var.v + eta*S_var_pref_v;


for k=1:1:K
    tot = 0;
    for s = 1:1:length(batch.v)
       j = batch.v(s);
       for t = 1:1:length(batch.u)
        j = batch.u(t);
        r = train.rating(i,j);
        if ( r > 0)
            tot = tot + 1;        
        end
       end
    end
    Rho_ksi.a(k) = (1-eta) * Rho_ksi.a(k) + eta*(kappa(1) + K*tot/2);
end


for k=1:1:K
    val = (1-eta)*Rho_ksi.b(k) + eta*kappa(2);
    for s=1:1:length(batch.v)
        j = batch.v(s);
        val = val + eta*(Qv.mean(k,j) )^2/2;
    
    end
    Rho_ksi.b(k) = val;
end


ind=1;

