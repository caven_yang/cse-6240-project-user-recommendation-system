function ind = updateDeltaList()

global delta_list


new_delta_u = delta_list.u;
new_delta_v = delta_list.v;


global TOT_RATER TOT_MOVIE

SIGMA_u = ones(TOT_RATER); SIGMA_v = ones(TOT_MOVIE);


for i=1:1:TOT_RATER
   for j=1:1:TOT_RATER
      SIGMA_u(i,j) = rowKernel(i,j);   
%       fprintf('SIGMA of u for (%d,%d) = %1.3f\n', i,j,SIGMA_u(i,j));
   end
end

% fprintf('SIGMA for u ready\n');

for i=1:1:TOT_MOVIE
   for j=1:1:TOT_MOVIE
      SIGMA_v(i,j) = columnKernel(i,j);
%       fprintf('SIGMA of v for (%d,%d) = %1.3f\n', i,j,SIGMA_v(i,j));
   end
end
% fprintf('SIGMA for v ready\n');

for i=1:1:length(delta_list.u)
   if( delta_list.u(i) ~= -10)
       new_delta_u(i) = uDelta(i,SIGMA_u);
   end
    
end

% fprintf('delta list for u ready\n');

for j=1:1:length(delta_list.v)
    if( delta_list.v(j) ~= -10)
        new_delta_v(j) = vDelta(j,SIGMA_v);
    end
    
end
% fprintf('delta list for v ready\n');

delta_list.u = new_delta_u;
delta_list.v = new_delta_v;

ind = 1;