function err = errorBar()

global pref Qu Qv test

RMSE = [];MAE = [];

for i = 1:1:length(test)
    u = test(i,1); v = test(i,2); r = test(i,3);
    diff = double(r)-pref.u(u)-pref.v(v) - (Qu.mean(:,u))'*Qv.mean(:,v);
    RMSE = [RMSE,diff*diff];
    MAE = [MAE,abs(diff)];
    
    
    
%     fprintf('%1.2f %1.2f %1.2f %1.2f\n',  test(i,3), pref.u(test(i,1)), pref.v(test(i,2)), (Qu.mean(:, test(i,1)))'*(Qv.mean(:,test(i,2))));
end

u = test(10,1); v = test(10,2); r = test(10,3);
% fprintf( 'one example: test = %1.3f\tprediction = %1.3f\n', r, pref.u(u)+pref.v(v)+(Qu.mean(:,u))'*(Qv.mean(:,v)));
RMSE = sqrt(mean(RMSE));

MAE =  mean(MAE);

fprintf('RMSE is %1.2f\tMAE is %1.2f\n', RMSE, MAE);


err = [double(RMSE),MAE];


