function d=uDkl(i,j)

global Qu K

PHI_i = diag(Qu.var(:,i));
PHI_j = diag(Qu.var(:,j));

mu = Qu.mean(:,j)-Qu.mean(:,i);



d=trace(inv(PHI_j)*PHI_i);
d=d+mu'*inv(PHI_j)*mu;
d=d+log(rank(PHI_i)/rank(PHI_j));
d=(d-K)/2;