function ind = updateParam_()

global batch Qu Qv train 

global S t PSI PHI tao2 K


for m=1:1:length(batch.u)
    i=batch.u(m);
    val = diag(Qu.var);
    for n=1:1:length(batch.v)
        j=batch.v(n);
        r=train.rating(i,j);
        if( r > 0)
           val = val + (PSI{j} + Qv.mean(:,j)*(Qv.mean(:,j))' )/tao2;            
        end
        
    end
    PHI{i} = inv(val);
    
    
    val = zeros(K,1);
    for n=1:1:length(batch.v)
      j=batch.v(n);
      r=train.rating(i,j);
      if( r > 0)
           val = val + double(r)*Qv.mean(:,j)/tao2;
           S{j} = S{j} + (PHI{i} + Qu.mean(:,i)*(Qu.mean(:,i))')/tao2;
           t{j} = t{j} + double(r)*Qu.mean(:,i)/tao2;
       
      end
        
    end
    Qu.mean(:,i) = PHI{i}*val;
end


for m=1:1:length(batch.v)
    j=batch.v(m);
    Qv.mean(:,j) = PSI{j}*t{j};
end
    
     
%    
% for k=1:1:K
%     val = 0;
%       for i=1:1:TOT_RATER
%          val = val + PHI{i} (k,k) + (Qu.mean(k,i))^2;       
%       end
%       
%      
%      val = val/(TOT_RATER-1);
%      Qu.var(k) = 1/val;
%        
% end




ind = 1;