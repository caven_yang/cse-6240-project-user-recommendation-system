function dd = vDelta(y,SIG)

global active TOT_MOVIE

a=SIG(y,y);

b=[];c=[];d=[];f=[];

B=[];C=[];

for z=1:1:TOT_MOVIE
    if( ismember(z, active.v))
        B = [B,z];
    elseif( z ~= y)
        C = [C,z];
    end
end



for i=1:1:length(B)
   b =  [b,SIG(y,B(i))];
   tmp=[];
   for j=1:1:length(B)
    tmp = [tmp,SIG(B(i), B(j))];  
   end
    d = [d;tmp];
end


for i=1:1:length(C)
   c = [c,SIG(y,C(i))];
   tmp =[];
   for j=1:1:length(C)
    tmp = [tmp,SIG(C(i),C(j))];   
   end
   f = [f;tmp];
end


dd=(a-b*pinv(d)*b')/(a-c*pinv(f)*c');