function ind = getBatch()

global delta_list batch active

global M sampling train


sorted = sort(delta_list.u,'descend');

chosen1 = find(delta_list.u >= sorted(M));
chosen2 = find(delta_list.u > sorted(M));

if(length(chosen1) == M)
    batch.u = chosen1;
else
    diff = setdiff(chosen1,chosen2);
    batch.u = [chosen2, diff(1:M-length(chosen2))];
end


sorted = sort(delta_list.v, 'descend');

chosen1 = find(delta_list.v >= sorted(M));
chosen2 = find(delta_list.v > sorted(M));

if(length(chosen1) == M)
    batch.v = chosen1;
else
    diff = setdiff(chosen1,chosen2);
    batch.v = [chosen2, diff(1:M-length(chosen2))];
end


delta_list.u(batch.u) = -10;
delta_list.v(batch.v) = -10;

active.u = sort([active.u, batch.u]);
active.v = sort([active.v, batch.v]);

tot = 0;
for x=1:1:length(batch.u)
    i=batch.u(x);
    for y = 1:1:length(batch.v)
        j=batch.v(y);
        r = train.rating(i,j);
        if(r>0)
            tot = tot + 1;
        end
    end
end

sampling=[sampling,tot];

ind = 1;