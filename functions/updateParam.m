function ind = updateParam()

global Qu Qv train batch

global K Vx Vs

global eta

new_var_u = Qu.var;
new_var_v = Qv.var;
slope_mean_u = Qu.mean;
slope_mean_v = Qv.mean;
new_Vs=Vs;
new_Vx = Vx;


for l=1:1:K
  for m = 1:1:length(batch.u) 
  i = batch.u(m);
   val = Qu.mean(l,i);
   for n=1:1:length(batch.v)
    j = batch.v(n);
    r = train.rating(i,j);
    if(r>0)
       tmp = r-(Qu.mean(:,i))'*Qv.mean(:,j);
       tmp = -1*tmp*Qv.mean(l,j);
       tmp = tmp + Qu.mean(l,i) * Qv.var(l,j);
       val = val + tmp/Vx;
    end
    
   end
   slope_mean_u(l,i) = val*eta + Qu.mean(l,i);
  end
end



for l=1:1:K
   for n = 1:1:length(batch.v) 
    j=batch.v(n);
    val = Qv.mean(l,j)/Vs(l);
    for m = 1:1:length(batch.u)
        i = batch.u(m);
        r = train.rating(i,j);
        if( r > 0)
            tmp = r - (Qu.mean(:,i))'*Qv.mean(:,j);
            tmp = -1*tmp*Qu.mean(l,i);
            tmp = tmp + Qu.var(l,i)*Qv.mean(l,j);
            val = val + tmp/Vx;
        end
    end
    slope_mean_v(l,j) = val*eta + Qv.mean(l,j);
   end
end


Qu.mean = slope_mean_u;
Qv.mean = slope_mean_v;




for k=1:1:K
   for n=1:1:length(batch.u) 
    i = batch.u(n);
    val = 1;
    for m=1:1:length(batch.v)
        j = batch.v(m);
        r = train.rating(i,j);
        if( r > 0)
           tmp = (Qv.mean(k,j))^2 +  Qv.var(k,j);
           val = val + tmp/Vx;
            
        end
    
    end
    new_var_u(k,i) = 1.0/val;
    
   end
end

for k=1:1:K
   for n=1:1:length(batch.v) 
       j = batch.v(n);
       val = 1/Vs(k);
       for m = 1:1:length(batch.u)
        i = batch.u(m);
        r = train.rating(i,j);
        if( r > 0)
           tmp = (Qu.mean(k,i) )^2 + Qu.var(k,i);
           tmp = tmp/Vx;
           val = val + tmp;
            
        end
           
       end
       new_var_v(k,j) = 1.0/val;
   end
end


tot = 0;
val = 0;

for m=1:1:length(batch.u)
    i = batch.u(m);
    for n = 1:1:length(batch.v)
        j = batch.v(n);
        r = train.rating(i,j);
        tmp = 0;
        if ( r > 0)
            tmp = r - (Qu.mean(:,i))'*Qv.mean(:,j);
            tmp = tmp*tmp;
            tot = tot + 1;
            for k=1:1:K
               tmp = tmp + (Qu.var(k,i))*(Qv.mean(k,j))^2; 
               tmp = tmp + (Qu.mean(k,i))^2*Qv.var(k,j);
               tmp = tmp + Qu.var(k,i)*Qv.var(k,j);
            end
        end
        val = val + tmp;
        
    end
end

new_Vx = val/tot;

for k = 1:1:K
    val = 0;
    tot = 0;
    for m=1:1:length(batch.v)
        j = batch.v(m);
        val = (Qv.mean(k,j))^2 + Qv.var(k,j);
%         val = val/2;
        tot = tot + 1;

    end
    new_Vs(k) = val/tot;
end

% 
% Vx = new_Vx;
% Vs = new_Vs;


Qu.var = new_var_u;
Qv.var = new_var_v;






ind = 1;