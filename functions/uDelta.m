function dd = uDelta(x,SIG)

global active TOT_RATER
global a b c d f B C

a=SIG(x,x);

b=[];c=[];d=[];f=[];

B=[];C=[];

for z=1:1:TOT_RATER
    if( ismember(z, active.u))
        B = [B,z];
    elseif( z ~= x)
        C = [C,z];
    end
end



for i=1:1:length(B)
   b =  [b,SIG(x,B(i))];
   tmp=[];
   for j=1:1:length(B)
    tmp = [tmp,SIG(B(i), B(j))];  
   end
    d = [d;tmp];
end


for i=1:1:length(C)
   c = [c,SIG(x,C(i))];
   tmp =[];
   for j=1:1:length(C)
    tmp = [tmp,SIG(C(i),C(j))];   
   end
   f = [f;tmp];
end


dd=(a-b*pinv(d)*b')/(a-c*pinv(f)*c');