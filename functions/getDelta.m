function delta = getDelta(i,which)



global U V
global SIGMA active

SIG_i_A = [];
SIG_A = [];
SIG_i_A_ = [];
SIG_A_=[];


list = [];
list_ = [];

switch (which)
    case U
        global TOT_RATER
        
        
        for p=1:1:TOT_RATER
            if( ismember(p,active.u) == 1 )
                list = [list,p];
            elseif ( p ~= i )
                list_ = [list_,p];
                
            end
            
            
        end

        SIG_i_A = ones(1,length(list));
        SIG_A = ones(length(list),length(list));
        SIG_i_A_ = ones(1,length(list_));
        SIG_A_ = ones(length(list_), length(list_));
        
        for p = 1:1:length(list)
           SIG_i_A(p) = SIGMA.u(i,list(p)); 
           for q = 1:1:length(list)              
              SIG_A(p,q) = SIGMA.u(list(p), list(q)); 
           end
            
        end
     
        for p = 1:1:length(list_)
           SIG_i_A_(p) = SIGMA.u(i,list_(p)); 
           for q = 1:1:length(list_)              
              SIG_A_(p,q) = SIGMA.u(list_(p), list_(q)); 
           end
            
        end
        
        
        delta = SIGMA.u(i,i) - SIG_i_A*pinv(SIG_A)*(SIG_i_A)';
        delta = delta/(SIGMA.u(i,i) - SIG_i_A_*pinv(SIG_A_)*(SIG_i_A_)');
        
        
    case V
        global TOT_MOVIE
        
        
        for p=1:1:TOT_MOVIE
            if( ismember(p,active.v) == 1 )
                list = [list,p];
            elseif ( p ~= i )
                list_ = [list_,p];
                
            end
            
            
        end

        SIG_i_A = ones(1,length(list));
        SIG_A = ones(length(list),length(list));
        SIG_i_A_ = ones(1,length(list_));
        SIG_A_ = ones(length(list_), length(list_));
        
        for p = 1:1:length(list)
           SIG_i_A(p) = SIGMA.v(i,list(p)); 
           for q = 1:1:length(list)              
              SIG_A(p,q) = SIGMA.v(list(p), list(q)); 
           end
            
        end
     
        for p = 1:1:length(list_)
           SIG_i_A_(p) = SIGMA.v(i,list_(p)); 
           for q = 1:1:length(list_)              
              SIG_A_(p,q) = SIGMA.v(list_(p), list_(q)); 
           end
            
        end
        
        
        delta = SIGMA.v(i,i) - SIG_i_A*pinv(SIG_A)*(SIG_i_A)';
        delta = delta/(SIGMA.v(i,i) - SIG_i_A_*pinv(SIG_A_)*(SIG_i_A_)');
        
        
        
end







