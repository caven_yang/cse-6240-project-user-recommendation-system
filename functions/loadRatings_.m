function ratings = loadRatings(fname)

global TOT_RATER TOT_MOVIE

ratings = struct('rating',zeros(TOT_RATER,TOT_MOVIE), 'timing', zeros(TOT_RATER, TOT_MOVIE));

fd = fopen(fname);

while(1)
    line = fgets(fd);
    
    if( ~ischar(line))
        break;
    end
    
    tokened = textscan(line,'%d\t%d\t%d\t%d');
    
    ratings.rating(tokened{1}, tokened{2}) = tokened{3};
    ratings.timing(tokened{1}, tokened{2}) = tokened{4};
%     fprintf('(%d,%d) = %d and %d\n', tokened{1}, tokened{2}, tokened{3}, tokened{4});
end

% clearvars ratings;
fprintf('loading complete\n');

fclose(fd);