function tests = loadTest_(fname)

fd = fopen(fname);
tests = [];

while(1)
    line = fgets(fd);
    
    if( ~ischar(line))
        break;
    end
    
%     tokened = textscan(line,'%d::%d::%d::%d');
    tokened = textscan(line,'%d\t%d\t%d\t%d');


    tests = [tests;tokened{1}, tokened{2}, tokened{3}, tokened{4}];
    
    
end



fclose(fd);