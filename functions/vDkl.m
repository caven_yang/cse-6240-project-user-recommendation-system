function d = vDkl(i,j)

global Qv K

KSI_i = diag(Qv.var(:,i));
KSI_j = diag(Qv.var(:,j));

mu = Qv.mean(:,j) - Qv.mean(:,i);

d=trace(inv(KSI_j)*KSI_i);
d=d+mu'*inv(KSI_j)*mu + log(rank(KSI_i)/rank(KSI_j));
d=(d-K)/2;