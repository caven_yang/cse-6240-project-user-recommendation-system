function err = errorBar_()

global pref Qu Qv test

RMSE = [];MAE = [];

for i = 1:1:length(test)
    u = test(i,1); v = test(i,2); r = test(i,3);
%     fprintf('u is %d\tv is %d\tr is %d\n',u,v,r);
    diff = double(r) - (Qu.mean(:,u))'*Qv.mean(:,v);
    RMSE = [RMSE,diff*diff];
    MAE = [MAE,abs(diff)];

%     fprintf('prediction is %1.3f\n', diff);
    
end

% fprintf( 'one example: test = %1.3f\tprediction = %1.3f\n', r, pref.u(u)+pref.v(v)+(Qu.mean(:,u))'*(Qv.mean(:,v)));
RMSE = sqrt(mean(RMSE));

MAE =  mean(MAE);


fprintf('RMSE is %1.2f\tMAE is %1.2f\n', RMSE, MAE);


err = [double(RMSE),MAE];


