fprintf('this is the project\n');
% clearvars
addpath('data/','functions/');

global TOT_RATER TOT_MOVIE
% TOT_RATER = 943;TOT_MOVIE = 1682;
TOT_RATER = 6040;TOT_MOVIE = 3952;


global train test

% train = loadRatings_('data/u1.base');
% test = loadTest_('data/u1.test');

train = loadRatings('data/m.train');
test = loadTest('data/m.test');


global K M

K = 20;
M = 40;

global Qu Qv
Qu = struct('mean',randi(1,K,TOT_RATER)*3/K, 'var', randi(1,K,1));
Qv = struct('mean',randi(1,K,TOT_MOVIE), 'var', randi(1,K,1));

global active SIGMA
active = struct('u', [], 'v', []);

SIGMA = struct('u', [], 'v', []);


global U V
U=1;V=2;

global delta_list
delta_list = struct('u', randi(100,1,TOT_RATER)/50, 'v', randi(100,1,TOT_MOVIE)/50 );


global eta decay sigma_epsilon

eta = -0.02; % initial value
decay = 0.9; % decay rate
sigma_epsilon = 1e-6;
error_bar = [];

S = cell(TOT_MOVIE); t = cell(TOT_MOVIE);

for j=1:1:TOT_MOVIE
    S{j} = diag(Qv.var);
    t{j} = zeros(K,1);
end


PSI = cell(TOT_MOVIE);
for j=1:1:TOT_MOVIE
   PSI{j} = diag(Qv.var); 
end

PHI = cell(TOT_RATER);
for i=1:1:TOT_RATER
   PHI{i} = diag(Qu.var); 
end
tao2 = 1;

t1 = clock;

for iteration = 1:1:13
   error_bar = [error_bar;errorBar_()];
   
%    for j=1:1:TOT_MOVIE
%     S{j} = diag(Qv.var);
%     t{j} = zeros(K,1);
%    end
   
   
   for i = 1:1:TOT_RATER 
    val = diag(Qu.var);
    for j=1:1:TOT_MOVIE
        r = train.rating(i,j);
        if( r > 0)
           val = val + (PSI{j} + Qv.mean(:,j)*(Qv.mean(:,j))' )/tao2;  
        end
    end
    PHI{i} = inv(val);
    
    val = zeros(K,1);
    for j=1:1:TOT_MOVIE
        r = train.rating(i,j);
        if( r > 0)
           val = val + r*Qv.mean(:,j)/tao2;
           S{j} = S{j} + (PHI{i} + Qu.mean(:,i)*(Qu.mean(:,i))')/tao2;
           t{j} = t{j} + r*Qu.mean(:,i)/tao2;
        end
    end
    Qu.mean(:,i) = PHI{i}*val;
   end
    
   
   for j=1:1:TOT_MOVIE
       PSI{j} = inv(S{j});
       Qv.mean(:,j) = PSI{j}*(t{j});
       
   end
   
   
   for k=1:1:K
      val = 0;
      for i=1:1:TOT_RATER
         val = val + PHI{i} (k,k) + (Qu.mean(k,i))^2;       
      end
      
     
     val = val/(TOT_RATER-1);
     Qu.var(k) = 1/val;
       
   end
   
   
%    val = 0;
%    for i=1:1:TOT_RATER
%     for j=1:1:TOT_MOVIE
%         r=train.rating(i,j);
%         if(r>0)
%             val = val + (r^2-2*r*((Qu.mean(:,i)')*Qv.mean(:,j)) );
%             val = val + trace((PHI{i}+(Qu.mean(:,i)*(Qu.mean(:,i))')) * (PSI{j}+(Qv.mean(:,j)*(Qv.mean(:,j))')));
%             
%         end
%     end
%    end
%    tao2 = val/(length(train.rating)-1);
       
end


t2 = clock;
fprintf('time used: %1.4f', etime(t2,t1));


