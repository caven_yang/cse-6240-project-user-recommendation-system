fprintf('this is the project\n');
% clearvars
addpath('data/','functions/');

global TOT_RATER TOT_MOVIE
% TOT_RATER = 6040;TOT_MOVIE = 3952;
TOT_RATER = 943;TOT_MOVIE = 1682;


global train test

train = loadRatings_('data/u1.base');
test = loadTest_('data/u1.test');

% train = loadRatings('data/u1.base');
% test = loadTest_('data/u1.test');

fprintf('loading done\n');

global K M

K = 5;
M = 200;

global Qu Qv
Qu = struct('mean',randi(1,K,TOT_RATER)-0.5, 'var', randi(1,K,TOT_RATER)/100);
Qv = struct('mean',randi(1,K,TOT_MOVIE)-0.5, 'var', randi(1,K,TOT_MOVIE)/100);

global delta_list batch active
delta_list = struct('u', randi(100,1, TOT_RATER), 'v', randi(100,1,TOT_MOVIE));
batch = struct('u',[], 'v', []);
active = struct('u',[],'v',[]);

global Vx Vs
Vx = 1;
Vs = ones(1,K);

global eta decay
eta = 2e-7;decay = 0.9;

global pref pref_var
pref = struct('u',zeros(1,TOT_RATER), 'v', zeros(1,TOT_MOVIE));
pref_var = struct('u',zeros(1,TOT_RATER), 'v', zeros(1,TOT_MOVIE));

global Rho_ksi TAO kappa sigma_epsilon
Rho_ksi = struct('a',ones(1,K), 'b',ones(1,K));
TAO = struct('a',1,'b',1);
kappa = [1e-6,1e-6];
sigma_epsilon = 1e-6;

error_bar = [];

t1=clock;
for epoch=1:1:100
    fprintf('for epoch %d\n',epoch);
    error_bar = [error_bar;errorBar()];
    
    active.u = []; active.v = [];
    delta_list.u = randi(100,1,TOT_RATER); delta_list.v = randi(100,1,TOT_MOVIE);
    for iteration = 1:1:100000
        fprintf( 'for iteration %i\n', iteration);
%         error_bar = [error_bar;errorBar_()];
        errorBar();
        getBatch();  % update active meanwhile
%         fprintf('active size is %d\tbatch size is %d\n', length(active.u), length(batch.u));
        if( length(active.u) > min(TOT_MOVIE,TOT_RATER) )
           break; 
        end
        
        updateParameters();
 

    end
    
end

t2=clock;
fprintf('time used is %1.4f\n', etime(t2,t1));
