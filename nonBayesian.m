fprintf('this is the project\n');
% clearvars
addpath('data/','functions/');

global TOT_RATER TOT_MOVIE
TOT_RATER = 943;TOT_MOVIE = 1682;
% TOT_RATER = 100; TOT_MOVIE = 100;


global train test

train = loadRatings_('data/u1.base');
test = loadTest_('data/u1.test');


global K M

K = 5;
M = 40;

global Qu Qv
Qu = struct('mean',3/(K)*ones(K,TOT_RATER), 'var', randi(1,K,TOT_RATER)-1);
Qv = struct('mean',ones(K,TOT_MOVIE), 'var', randi(1,K,TOT_MOVIE)-1);

global active SIGMA
active = struct('u', [], 'v', []);

SIGMA = struct('u', [], 'v', []);


global U V
U=1;V=2;

global delta_list
delta_list = struct('u', randi(100,1,TOT_RATER)/50, 'v', randi(100,1,TOT_MOVIE)/50 );


global eta decay sigma_epsilon

eta = -0.02; % initial value
decay = 0.9; % decay rate
sigma_epsilon = 1e-6;
error_bar = [];
B=10;

t1 = clock;
for iteration = 1:1:100
    fprintf( 'for iteration %i\n', iteration);
    error_bar = [error_bar;errorBar_()];
    
 
    for i = 1:1:TOT_RATER
       
       for j = 1:1:TOT_MOVIE
           if ( train.rating(i,j) > 0 )
               new_u_mean = (Qu.mean(:, i) - eta*(train.rating(i,j) - (Qu.mean(:,i))'*(Qv.mean(:,j)))*Qv.mean(:,j));
               new_v_mean = (Qv.mean(:, j) - eta*(train.rating(i,j) - (Qu.mean(:,i))'*(Qv.mean(:,j)))*Qu.mean(:,i));
               if ( norm_l2(new_u_mean) >= B )
                    Qu.mean(:,i) = new_u_mean/sqrt(B);
               else
                    Qu.mean(:,i) = new_u_mean;
               end
               if ( norm_l2(new_v_mean) >= B )
                    Qv.mean(:,j) = new_v_mean/sqrt(B);
               else
                    Qv.mean(:,j) = new_v_mean;
               end
               
               
           end
       end
    end
    
    
    
    
end

t2 = clock;
fprintf('time used: %1.4f', etime(t2,t1));