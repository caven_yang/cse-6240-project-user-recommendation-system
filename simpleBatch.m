fprintf('this is the project\n');
% clearvars
addpath('data/','functions/');

global TOT_RATER TOT_MOVIE
% TOT_RATER = 6040;TOT_MOVIE = 3952;
TOT_RATER = 943;TOT_MOVIE = 1682;


global train test

train = loadRatings_('data/u1.base');
test = loadTest_('data/u1.test');

% train = loadRatings('data/u1.base');
% test = loadTest_('data/u1.test');

fprintf('loading done\n');

global K M

K = 5;
M = 100;

global Qu Qv
Qu = struct('mean',randi(1,K,TOT_RATER)-1, 'var', randi(1,K,1)/100);
Qv = struct('mean',randi(1,K,TOT_MOVIE), 'var', randi(1,K,1)/100);

global delta_list batch active
delta_list = struct('u', randi(100,1, TOT_RATER), 'v', randi(100,1,TOT_MOVIE));
batch = struct('u',[], 'v', []);
active = struct('u',[],'v',[]);

global S t
S = cell(TOT_MOVIE); t = cell(TOT_MOVIE);

for j=1:1:TOT_MOVIE
    S{j} = diag(Qv.var);
    t{j} = zeros(K,1);
end

global PSI PHI tao2

PSI = cell(TOT_MOVIE);
for j=1:1:TOT_MOVIE
   PSI{j} = diag(Qv.var); 
end

PHI = cell(TOT_RATER);
for i=1:1:TOT_RATER
   PHI{i} = diag(Qu.var); 
end
tao2 = 1;
error_bar = [];

t1=clock;
for epoch=1:1:3
    fprintf('for epoch %d\n',epoch);
%     error_bar = [error_bar;errorBar_()];
    
    active.u = []; active.v = [];
    delta_list.u = randi(100,1,TOT_RATER); delta_list.v = randi(100,1,TOT_MOVIE);
    for iteration = 1:1:100000
        fprintf( 'for iteration %i\n', iteration);
        error_bar = [error_bar;errorBar_()];
%         errorBar_();
        getBatch();  % update active meanwhile
        if( length(active.u) > min(TOT_MOVIE,TOT_RATER) )
           break; 
        end
        updateParam_(); 


    end
    
end

t2=clock;
fprintf('time used is %1.4f\n', etime(t2,t1));
