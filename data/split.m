fprintf('to split the data\n');
addpath('../functions/');

TOT_RATER = 6040;TOT_MOVIE = 3900;
data = fopen('ratings.dat');

train = fopen('m.train', 'w+');
test = fopen('m.test','w+');

while (1)
    line = fgets(data);
    
    if( ~ischar(line))
        break;
    end
    
    
    if(randi(101,1,1)-1 > 10)
        fprintf(train,line);
    else
        fprintf(test,line);
    
    end
    
    
end

fclose(data);

fclose(train);
fclose(test);